# OwlyEggs

## Was ist OwlyEggs?
OwlyEggs ist eine super erlaubte automatische Anwendung, die zum WoW Oster-Event entwickelt wurde. Durch Owly Eggs sollen Eier unauffällig automatisch eingesammelt werden. Bisher wird ein Eierspawn zur Zeit unterstützt.

#Wie benutze ich OwlyEggs?
* F1: Speichere Position 1
* F2: Speichere Position 2
* F3: Speichere Position 3
* F4: Speichere Position 4
* F5: Start / Stop Multi-Position-Klick
* F6: Start / Stop Single-Position-Klick
* F12: Anwendung beenden
* Pfeil-Links: Zeit zwischen den Klicks verringern (Nur für Single-Position)
* Pfeil-Rechts: Zeit zwischen den Klicks erhöhen (Nur für Single-Position)

#Wie funktioniert der Multi-Positions-Klick?
1. Makro "/use Bunt gefärbtes Ei" auf die 1 legen, damit die Eier automatisch extrahiert werden können
2. Mit den Tasten F1-F3 die gewünschten Positionen festlegen (es wird mindestens eine Position benötigt)
3. Automatismus mit F5 starten

#Tipps und Tricks
Da die Eier offenbar nicht zeitlich spawnen, sondern je nach Bedarf kommen, dürfte das parallele Arbeiten mit einem Freund den Vorgang optimieren.

#Wichtig
* Der Schreiter kann erst ab **Level 20** erworben werden!