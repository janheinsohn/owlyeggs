package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.framework.ioc.Provider;
import heinsohn.jan.owlyeggs.framework.logging.Logger;
import heinsohn.jan.owlyeggs.framework.threading.IKillableRunnable;
import heinsohn.jan.owlyeggs.logic.contracts.IKeyboardHandler;
import heinsohn.jan.owlyeggs.logic.contracts.IMouseHandler;
import heinsohn.jan.owlyeggs.logic.models.MousePosition;

public class MultiPositionRunnable implements Runnable, IKillableRunnable {
    private MousePosition positionOne;
    private MousePosition positionTwo;
    private MousePosition positionThree;
    private IMouseHandler mouseHandler;
    private IKeyboardHandler keyboardHandler;
    private boolean stopped = false;
    private int clicksPerRun = 100;

    private boolean secondPositionActivated = false;
    private boolean thirdPositionActivated = false;

    MultiPositionRunnable(MousePosition positionOne, MousePosition positionTwo, MousePosition positionThree) {
        this.positionOne = positionOne;

        if(positionTwo != null) {
            this.positionTwo = positionTwo;
            secondPositionActivated = true;
        }

        if(positionThree != null) {
            this.positionThree = positionThree;
            thirdPositionActivated = true;
        }

        mouseHandler = Provider.getInstance().provide("mouseHandler");
        keyboardHandler = Provider.getInstance().provide("keyboardHandler");
    }

    @Override
    public void run() {
        int runs = 0; // Wird beim Logging hochgezählt
        while (true) {
            runs = 0;
            for(int i = 0; i < clicksPerRun; i++){
                if(stopped) return;
                Logger.getInstance().writeInformation(String.format("Starte Durchlauf %d / %d", ++runs, clicksPerRun), "d203d499-ce80-49a4-83d8-22506ebd0053");
                try {
                    performMoveClick(positionOne);
                    Thread.sleep(1400);
                    if(secondPositionActivated) {
                        performMoveClick(positionTwo);
                        Thread.sleep(1400);
                    }
                    if(thirdPositionActivated) {
                        performMoveClick(positionThree);
                        Thread.sleep(1400);
                    }
                } catch (InterruptedException e) {
                    Logger.getInstance().writeError("Fehler beim Pausieren zwischen Rechtsklick und Movement.", e, "cd07702b-0faa-4c30-8a22-9f88e049059d");
                }
            }
            if(stopped) return;
            Logger.getInstance().writeInformation("Öffne Eier...", "7aec5a9e-f190-459c-b019-3a659a4dbf4b");
            for(int i = 0; i < 50; i++) {
                if(stopped) return;
                keyboardHandler.pressOne();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    Logger.getInstance().writeError("Fehler beim Pausieren zwischen dem Aufmachen eines Ei.", e, "4700e95c-0dfb-453d-85ff-f8bbc7c5449b");
                }
            }
        }
    }

    private void performMoveClick(MousePosition position) {
        Logger.getInstance().writeVerbose(String.format("Bewege Maus zu Poisiton 1: %d , %d", position.getX(), position.getY()), "958c5977-d08f-4f45-94bb-a2f536421072");
        for(int j = 0; j < 3; j++)//while(mouseHandler.getCurrentMousePosition().getX() != positionOne.getX() || mouseHandler.getCurrentMousePosition().getY() != positionOne.getY())
            mouseHandler.moveMouse(position);
        Logger.getInstance().writeVerbose(String.format("Aktuelle Mausposition: %d , %d", mouseHandler.getCurrentMousePosition().getX(), mouseHandler.getCurrentMousePosition().getY()), "31b4b2d1-c148-4a8e-8194-276de0d1aeb0");

        Logger.getInstance().writeVerbose("Rechtsklicken wird durchgeführt.", "84f2ad51-7e54-4a57-919c-0f7e8f30ee92");
        mouseHandler.performRightClick();
    }

    @Override
    public void stop() {
        Logger.getInstance().writeInformation("MultiPosition-Schleife wird beendet.", "583bc1a9-03bc-42a2-a1fb-ed1cea6b744a");
        stopped = true;
    }
}
