package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.framework.threading.IKillableRunnable;
import heinsohn.jan.owlyeggs.logic.models.MousePosition;

import java.awt.*;

public class MouseLockingRunnable implements Runnable, IKillableRunnable {
    private MousePosition mousePosition;
    private boolean lockMouse = true;
    private Robot robot;


    public MouseLockingRunnable(MousePosition mousePosition, Robot robot) {
        this.mousePosition = mousePosition;
        this.robot = robot;
    }

    @Override
    public void run() {
        while (lockMouse) {
            robot.mouseMove(mousePosition.getX(), mousePosition.getY());
        }
    }

    @Override
    public void stop() {
        lockMouse = false;
    }
}
