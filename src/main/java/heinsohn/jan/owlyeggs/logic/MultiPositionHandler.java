package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.framework.logging.Logger;
import heinsohn.jan.owlyeggs.logic.contracts.IEventAggregator;
import heinsohn.jan.owlyeggs.logic.contracts.IMultiPositionHandler;
import heinsohn.jan.owlyeggs.logic.models.MousePosition;
import heinsohn.jan.owlyeggs.ui.models.BunnyColor;
import sun.rmi.runtime.Log;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MultiPositionHandler implements IMultiPositionHandler {
    private MousePosition positionOne;
    private MousePosition positionTwo;
    private MousePosition positionThree;
    private MultiPositionRunnable multiPositionRunnable;
    private ExecutorService executorService;
    private IEventAggregator eventAggregator;

    public MultiPositionHandler(IEventAggregator eventAggregator) {
        this.eventAggregator = eventAggregator;
        executorService = Executors.newSingleThreadExecutor();
    }


    @Override
    public void setPositionOne(MousePosition mousePosition) {
        positionOne = mousePosition;
        Logger.getInstance().writeInformation("Mausposition 1 wurde erfolgreich gespeichert.", "73c40bf5-7634-4060-b316-395419da2706");
    }

    @Override
    public void setPositionTwo(MousePosition mousePosition) {
        positionTwo = mousePosition;
        Logger.getInstance().writeInformation("Mausposition 2 wurde erfolgreich gespeichert.", "d8e64359-8f5f-4c76-8dd2-ec01e8f5119b");
    }

    @Override
    public void setPositionThree(MousePosition mousePosition) {
        positionThree = mousePosition;
        Logger.getInstance().writeInformation("Mausposition 3 wurde erfolgreich gespeichert.", "1c9da9cc-1aa8-481b-aa28-6b715f3d1bbf");
    }

    @Override
    public void start() {
        Logger.getInstance().writeInformation("Multipositionsclicking wird gestartet.", "faa971db-4882-40cc-a5b8-7be84e9bc43e");
        if(positionOne == null) {
            Logger.getInstance().writeWarning("Es wird mindestens eine Position benötigt.", "a2419f1c-b2b6-4658-9ca5-ab19b58e2d1b");
            return;
        }

        eventAggregator.changeBunnyColor(BunnyColor.Blue);

        multiPositionRunnable = new MultiPositionRunnable(positionOne, positionTwo, positionThree);
        executorService.submit(multiPositionRunnable);
    }

    @Override
    public void stop() {
        Logger.getInstance().writeInformation("Multipositionsclicking wird gestoppt.", "707c0161-b893-40fb-ace6-150fed71158f");
        multiPositionRunnable.stop();
        eventAggregator.changeBunnyColor(BunnyColor.Pink);
    }
}
