package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.logic.contracts.IAutomationHandler;
import heinsohn.jan.owlyeggs.logic.contracts.IEventAggregator;
import heinsohn.jan.owlyeggs.logic.contracts.IMouseHandler;
import heinsohn.jan.owlyeggs.logic.contracts.IMultiPositionHandler;
import heinsohn.jan.owlyeggs.logic.models.AutomationType;
import heinsohn.jan.owlyeggs.logic.models.MousePosition;
import heinsohn.jan.owlyeggs.ui.models.BunnyColor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AutomationHandler implements IAutomationHandler {
    private MousePosition mousePosition;
    private ExecutorService executorService;
    private MouseClickingRunnable mouseClickingRunnable;
    private IEventAggregator eventAggregator;
    private IMultiPositionHandler multiPositionHandler;


    public AutomationHandler(IEventAggregator eventAggregator, IMultiPositionHandler multiPositionHandler) {
        this.eventAggregator = eventAggregator;
        this.multiPositionHandler = multiPositionHandler;

        executorService = Executors.newSingleThreadExecutor();
    }

    @Override
    public void startNewAutomation(AutomationType type) {
        switch (type) {

            case RightClickOnPoint:
                startNewRightClickAtPointRunnable();
                break;
            case MultiPosition:
                break;
            case One:
                break;
        }

    }


    @Override
    public void stopAll() {
        if(mouseClickingRunnable != null) {
            mouseClickingRunnable.stop();
            mouseClickingRunnable = null;
        }

        eventAggregator.changeBunnyColor(BunnyColor.Pink);
    }

    @Override
    public void multiPositionSafeMouseOne(MousePosition mousePosition) {
        multiPositionHandler.setPositionOne(mousePosition);
    }

    @Override
    public void multiPositionSafeMouseTwo(MousePosition mousePosition) {
        multiPositionHandler.setPositionTwo(mousePosition);
    }

    private void startNewRightClickAtPointRunnable() {
        if(mouseClickingRunnable != null) return;
        eventAggregator.changeBunnyColor(BunnyColor.Blue);

        mouseClickingRunnable = new MouseClickingRunnable();
        executorService.submit(mouseClickingRunnable);
    }
}
