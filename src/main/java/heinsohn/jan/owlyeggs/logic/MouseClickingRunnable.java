package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.framework.ioc.Provider;
import heinsohn.jan.owlyeggs.framework.threading.IKillableRunnable;
import heinsohn.jan.owlyeggs.framework.threading.RunnableKiller;
import heinsohn.jan.owlyeggs.logic.contracts.IConfigurationHandler;
import heinsohn.jan.owlyeggs.logic.contracts.IMouseHandler;
import heinsohn.jan.owlyeggs.logic.models.MousePosition;

public class MouseClickingRunnable implements Runnable, IKillableRunnable {
    private int interval;
    private MousePosition mousePosition;
    private boolean stopped = false;
    private boolean withFixedPosition;

    MouseClickingRunnable() {
        RunnableKiller.getInstance().register(this);
        IConfigurationHandler configurationHandler = Provider.getInstance().provide("configurationHandler");
        this.interval = configurationHandler.getMouseClickingInterval();
    }


    @Override
    public void run() {
        IMouseHandler mouseHandler = Provider.getInstance().provide("mouseHandler");
        while (!stopped) {
            if(withFixedPosition)
                mouseHandler.performRightClickAtPoint(mousePosition);
            else
                mouseHandler.performRightClick();
            try {
                for(int i = 0; i < interval / 500; i++) {
                    Thread.sleep(500);
                    if(stopped) return;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void stop() {
        stopped = true;
    }
}
