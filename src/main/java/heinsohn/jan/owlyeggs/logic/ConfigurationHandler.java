package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.logic.contracts.IConfigurationHandler;

public class ConfigurationHandler implements IConfigurationHandler {
    private int mouseClickingInterval;
    private int mouseXOffset;
    private int mouseYOffset;

    @Override
    public int getMouseClickingInterval() {
        return mouseClickingInterval;
    }

    @Override
    public void setMouseClickingInterval(int mouseClickingInterval) {
        this.mouseClickingInterval = mouseClickingInterval;
    }

    @Override
    public void setMouseXOffset(int mouseXOffset) {
        this.mouseXOffset = mouseXOffset;
    }

    @Override
    public void setMouseYOffset(int mouseYOffset) {
        this.mouseYOffset = mouseYOffset;
    }

    @Override
    public int getMouseXOffset() {
        return mouseXOffset;
    }

    @Override
    public int getMouseY() {
        return mouseYOffset;
    }
}
