package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.framework.logging.Logger;
import heinsohn.jan.owlyeggs.logic.contracts.IMouseHandler;
import heinsohn.jan.owlyeggs.logic.models.MousePosition;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MouseHandler implements IMouseHandler{
    private Robot robot;
    private ExecutorService executorService;
    private MouseLockingRunnable mouseLockingRunnable;

    public MouseHandler() {
        executorService = Executors.newSingleThreadExecutor();
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public void performRightClickAtPoint(MousePosition position) {
        moveMouse(position);
        performRightClick();

    }

    @Override
    public void performRightClick() {
        robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
    }

    @Override
    public void lockMouse() {
        mouseLockingRunnable = new MouseLockingRunnable(getCurrentMousePosition(), robot);
        executorService.submit(mouseLockingRunnable);
    }

    @Override
    public void unlockMouse() {
        mouseLockingRunnable.stop();
    }

    @Override
    public void moveMouse(MousePosition mousePosition) {
//        Logger.getInstance().writeVerbose(String.format("Bewege Maus zu: %d , %d", mousePosition.getX(), mousePosition.getY()), "4aea9a90-12f0-470c-bc29-2a287098f48a");
        robot.mouseMove(mousePosition.getX(), mousePosition.getY());
    }


    public MousePosition getCurrentMousePosition() {
        PointerInfo a = MouseInfo.getPointerInfo();
        Point b = a.getLocation();
        int x = (int) b.getX();
        int y = (int) b.getY();
        return new MousePosition(x, y);
    }

}