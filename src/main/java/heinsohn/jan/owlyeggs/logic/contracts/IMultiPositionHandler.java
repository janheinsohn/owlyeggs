package heinsohn.jan.owlyeggs.logic.contracts;

import heinsohn.jan.owlyeggs.logic.models.MousePosition;

public interface IMultiPositionHandler {
    void setPositionOne(MousePosition mousePosition);
    void setPositionTwo(MousePosition mousePosition);
    void setPositionThree(MousePosition mousePosition);
    void start();
    void stop();
}
