package heinsohn.jan.owlyeggs.logic.contracts;

import heinsohn.jan.owlyeggs.ui.contracts.IBunnyScreen;
import heinsohn.jan.owlyeggs.ui.models.BunnyColor;

public interface IEventAggregator {
    void registerBunnyListener(IBunnyScreen bunnyScreen);
    void changeBunnyColor(BunnyColor bunnyColor);
}
