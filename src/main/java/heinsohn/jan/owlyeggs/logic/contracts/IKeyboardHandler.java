package heinsohn.jan.owlyeggs.logic.contracts;

public interface IKeyboardHandler {
    void registerForKeyboardEvents();
    void pressOne();

}
