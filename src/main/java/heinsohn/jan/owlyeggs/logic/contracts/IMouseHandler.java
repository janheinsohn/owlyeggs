package heinsohn.jan.owlyeggs.logic.contracts;

import heinsohn.jan.owlyeggs.logic.models.MousePosition;

public interface IMouseHandler {
    void performRightClickAtPoint(MousePosition position);
    void performRightClick();
    void lockMouse();
    void unlockMouse();
    void moveMouse(MousePosition mousePosition);
    MousePosition getCurrentMousePosition();
}
