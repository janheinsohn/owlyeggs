package heinsohn.jan.owlyeggs.logic.contracts;

import heinsohn.jan.owlyeggs.logic.models.AutomationType;
import heinsohn.jan.owlyeggs.logic.models.MousePosition;

public interface IAutomationHandler {
    void startNewAutomation(AutomationType type);
    void stopAll();
    void multiPositionSafeMouseOne(MousePosition mousePosition);
    void multiPositionSafeMouseTwo(MousePosition mousePosition);
}
