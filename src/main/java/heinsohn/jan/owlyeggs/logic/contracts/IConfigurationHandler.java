package heinsohn.jan.owlyeggs.logic.contracts;

public interface IConfigurationHandler {
    int getMouseClickingInterval();
    void setMouseClickingInterval(int mouseClickingInterval);
    void setMouseXOffset(int mouseXOffset);
    void setMouseYOffset(int mouseYOffset);
    int getMouseXOffset();
    int getMouseY();
}
