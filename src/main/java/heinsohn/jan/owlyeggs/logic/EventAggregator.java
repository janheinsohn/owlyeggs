package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.logic.contracts.IEventAggregator;
import heinsohn.jan.owlyeggs.ui.contracts.IBunnyScreen;
import heinsohn.jan.owlyeggs.ui.models.BunnyColor;

import java.util.ArrayList;

public class EventAggregator implements IEventAggregator {
    private ArrayList<IBunnyScreen> bunnyListeners;

    public EventAggregator() {
        bunnyListeners = new ArrayList();
    }

    @Override
    public void registerBunnyListener(IBunnyScreen bunnyListener) {
        bunnyListeners.add(bunnyListener);
    }

    @Override
    public void changeBunnyColor(BunnyColor bunnyColor) {
        bunnyListeners.forEach(x -> x.changeColor(bunnyColor));
    }
}
