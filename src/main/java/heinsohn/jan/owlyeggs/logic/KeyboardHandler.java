package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.logic.contracts.*;
import heinsohn.jan.owlyeggs.logic.models.AutomationType;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KeyboardHandler implements IKeyboardHandler, NativeKeyListener {
    private IAutomationHandler automationHandler;
    private IConfigurationHandler configurationHandler;
    private IMultiPositionHandler multiPositionHandler;
    private IMouseHandler mouseHandler;
    private boolean mouseClickingToogle = false;
    private boolean multiPositionToggle = false;

    public KeyboardHandler(IAutomationHandler automationHandler, IConfigurationHandler configurationHandler, IMultiPositionHandler multiPositionHandler, IMouseHandler mouseHandler) {
        this.automationHandler = automationHandler;
        this.configurationHandler = configurationHandler;
        this.multiPositionHandler = multiPositionHandler;
        this.mouseHandler = mouseHandler;
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F6) {
            if(mouseClickingToogle)
                automationHandler.stopAll();
            else
                automationHandler.startNewAutomation(AutomationType.RightClickOnPoint);
            mouseClickingToogle = !mouseClickingToogle;
        }

        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F12) {
            System.exit(0);
        }


        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F1) {
            multiPositionHandler.setPositionOne(mouseHandler.getCurrentMousePosition());
        }
        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F2) {
            multiPositionHandler.setPositionTwo(mouseHandler.getCurrentMousePosition());
        }
        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F3) {
            multiPositionHandler.setPositionThree(mouseHandler.getCurrentMousePosition());
        }
        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F5) {
            if (!multiPositionToggle) multiPositionHandler.start();
            else multiPositionHandler.stop();
            multiPositionToggle = !multiPositionToggle;
        }

        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_RIGHT) {
            configurationHandler.setMouseClickingInterval(configurationHandler.getMouseClickingInterval() + 100);

        }

        if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_LEFT) {
            if(configurationHandler.getMouseClickingInterval() > 100 )
                configurationHandler.setMouseClickingInterval(configurationHandler.getMouseClickingInterval() - 100);
        }



    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void registerForKeyboardEvents() {
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
        }
        GlobalScreen.addNativeKeyListener(this);

        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);

        logger.setUseParentHandlers(false);
    }

    @Override
    public void pressOne() {
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        assert robot != null;
        robot.keyPress(KeyEvent.VK_1);
        robot.keyRelease(KeyEvent.VK_1);
    }
}
