package heinsohn.jan.owlyeggs.logic.models;

public enum AutomationType {
    RightClickOnPoint,
    MultiPosition,
    One
}
