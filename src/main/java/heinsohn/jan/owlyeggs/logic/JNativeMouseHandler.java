package heinsohn.jan.owlyeggs.logic;

import heinsohn.jan.owlyeggs.logic.contracts.IMouseHandler;
import heinsohn.jan.owlyeggs.logic.models.MousePosition;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;

import java.awt.*;
import java.awt.event.InputEvent;

/**
 * @deprecated Lediglich ein Versuchsobjekt
 */
public class JNativeMouseHandler implements IMouseHandler, NativeMouseInputListener {
    private MousePosition currentMousePosition;
    private Robot robot;

    public JNativeMouseHandler() {
        currentMousePosition = new MousePosition(0, 0);
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        try {
            GlobalScreen.registerNativeHook();
        }
        catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook."); //TODO Log
        }
        GlobalScreen.addNativeMouseListener(this);
        GlobalScreen.addNativeMouseMotionListener(this);

    }

    @Override
    public void performRightClickAtPoint(MousePosition position) {
        moveMouse(position);
        performRightClick();
    }

    @Override
    public void performRightClick() {
        robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
    }

    @Override
    public void lockMouse() {

    }

    @Override
    public void unlockMouse() {

    }

    @Override
    public void moveMouse(MousePosition mousePosition) {
        robot.mouseMove(mousePosition.getX(), mousePosition.getY());
    }

    @Override
    public MousePosition getCurrentMousePosition() {
        return new MousePosition(currentMousePosition.getX(), currentMousePosition.getY());
    }

    @Override
    public void nativeMouseClicked(NativeMouseEvent nativeMouseEvent) {
        //Ignored
    }

    @Override
    public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {
        //Ignored
    }

    @Override
    public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) {
        //Ignored
    }

    @Override
    public void nativeMouseMoved(NativeMouseEvent nativeMouseEvent) {
        currentMousePosition.setX(nativeMouseEvent.getX());
        currentMousePosition.setY(nativeMouseEvent.getY());
    }

    @Override
    public void nativeMouseDragged(NativeMouseEvent nativeMouseEvent) {

    }
}
