package heinsohn.jan.owlyeggs.ui.models;

public enum BunnyColor {
    //TODO Bildnamen der Farben eintragen
    Blue(""),
    Pink("");

    private final String fieldDescription;

    BunnyColor(String value) {
        fieldDescription = value;
    }

    public String getFieldDescription() {
        return fieldDescription;
    }
}
