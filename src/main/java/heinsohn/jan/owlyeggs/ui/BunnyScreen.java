package heinsohn.jan.owlyeggs.ui;

import heinsohn.jan.owlyeggs.framework.ioc.Provider;
import heinsohn.jan.owlyeggs.logic.contracts.IEventAggregator;
import heinsohn.jan.owlyeggs.ui.contracts.IBunnyScreen;
import heinsohn.jan.owlyeggs.ui.models.BunnyColor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public class BunnyScreen implements IBunnyScreen {
    private JFrame frame;
    private JLabel imageLabel;

    public BunnyScreen() {
        ((IEventAggregator) Provider.getInstance().provide("eventAggregator")).registerBunnyListener(this);
    }

    @Override
    public void bootstrap() {

        frame = new JFrame("OwlyEggs");
        frame.setUndecorated(true);
        frame.setSize(400, 400);
        frame.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
        frame.getContentPane().setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setAlwaysOnTop(true);

        imageLabel = new JLabel((new ImageIcon(new ImageIcon(getClass().getResource("/bunnys/OwlyEggsBunnyPink.png")).getImage().getScaledInstance(400, 400, Image.SCALE_SMOOTH))));
        frame.add(imageLabel);
        imageLabel.setLayout(new FlowLayout());
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    @Override
    public void show() {
        frame.setVisible(true);
    }

    @Override
    public void hide() {
        frame.setVisible(false);
    }

    @Override
    public void close() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    @Override
    public void changeColor(BunnyColor bunnyColor) {
        switch (bunnyColor) {
            case Blue:
                imageLabel.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/bunnys/OwlyEggsBunnyBlue.png")).getImage().getScaledInstance(400, 400, Image.SCALE_SMOOTH)));
                break;
            case Pink:
                imageLabel.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/bunnys/OwlyEggsBunnyPink.png")).getImage().getScaledInstance(400, 400, Image.SCALE_SMOOTH)));
                break;
        }

    }

}