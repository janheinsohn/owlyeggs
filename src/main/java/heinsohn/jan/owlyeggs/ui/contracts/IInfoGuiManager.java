package heinsohn.jan.owlyeggs.ui.contracts;

public interface IInfoGuiManager {
    void show();
    void hide();
}
