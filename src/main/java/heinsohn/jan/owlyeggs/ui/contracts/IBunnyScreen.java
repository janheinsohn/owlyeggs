package heinsohn.jan.owlyeggs.ui.contracts;

import heinsohn.jan.owlyeggs.ui.models.BunnyColor;

public interface IBunnyScreen {
    void bootstrap();
    void show();
    void hide();
    void close();

    void changeColor(BunnyColor bunnyColor);


}
