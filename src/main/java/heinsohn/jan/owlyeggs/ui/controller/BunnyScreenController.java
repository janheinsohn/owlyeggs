package heinsohn.jan.owlyeggs.ui.controller;

import heinsohn.jan.owlyeggs.ui.BunnyScreen;
import heinsohn.jan.owlyeggs.ui.contracts.IBunnyScreen;

public class BunnyScreenController {
    private IBunnyScreen view;

    public BunnyScreenController() {
        view = new BunnyScreen();
    }

    private void showView() {
        view.show();
    }

    public void start() {
        view.bootstrap();
        showView();
    }
}
