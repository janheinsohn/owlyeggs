package heinsohn.jan.owlyeggs;

import heinsohn.jan.owlyeggs.framework.ioc.Provider;
import heinsohn.jan.owlyeggs.framework.logging.BasicLoggerImplementation;
import heinsohn.jan.owlyeggs.framework.logging.Logger;
import heinsohn.jan.owlyeggs.logic.contracts.IKeyboardHandler;
import heinsohn.jan.owlyeggs.ui.controller.BunnyScreenController;

/**
 * Startpunkt der Anwendung
 */
public class Startup {
    /**
     * Startmethode der Hauptanwendung
     * BunnyScreen wird geöffnet, welcher den aktuellen Status symbolisiert.
     * Keyboard Shortcuts werden initialisiert.
     * @param args Nimmt keine Argumente entgegen
     */
    public static void main(String[] args) {
        //Logging
        Logger.getInstance().registerLoggingListener(new BasicLoggerImplementation());

        //Bunny Screen
        BunnyScreenController bunnyScreenController = new BunnyScreenController();
        bunnyScreenController.start();

        //Keyboard
        IKeyboardHandler keyboardHandler = Provider.getInstance().provide("keyboardHandler");
        keyboardHandler.registerForKeyboardEvents();
    }
}
