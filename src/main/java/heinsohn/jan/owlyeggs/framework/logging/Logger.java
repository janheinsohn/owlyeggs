package heinsohn.jan.owlyeggs.framework.logging;

import heinsohn.jan.owlyeggs.framework.logging.contracts.ILogging;
import heinsohn.jan.owlyeggs.framework.logging.contracts.ILoggingListener;

import java.util.ArrayList;

public class Logger implements ILogging {
    private static Logger ourInstance = new Logger();

    public static Logger getInstance() {
        return ourInstance;
    }

    private Logger() {
        loggingListeners = new ArrayList<>();
    }

    private ArrayList<ILoggingListener> loggingListeners;

    public void registerLoggingListener(ILoggingListener loggingListener) {
        loggingListeners.add(loggingListener);
    }

    @Override
    public void writeVerbose(String log, String id) {
        loggingListeners.forEach(x -> x.writeVerbose(log, id));
    }

    @Override
    public void writeInformation(String log, String id) {
        loggingListeners.forEach(x -> x.writeInformation(log, id));
    }

    @Override
    public void writeWarning(String log, String id) {
        loggingListeners.forEach(x -> x.writeWarning(log, id));
    }

    @Override
    public void writeError(String log, Exception e, String id) {
        loggingListeners.forEach(x -> x.writeError(log, e,  id));
    }
}
