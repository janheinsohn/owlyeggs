package heinsohn.jan.owlyeggs.framework.logging.contracts;

public interface ILogging {
    void writeVerbose(String log, String id);
    void writeInformation(String log, String id);
    void writeWarning(String log, String id);
    void writeError(String log, Exception e, String id);
}
