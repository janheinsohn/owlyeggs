package heinsohn.jan.owlyeggs.framework.logging;

import heinsohn.jan.owlyeggs.framework.logging.contracts.ILoggingListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BasicLoggerImplementation implements ILoggingListener {

    private void log(String log) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(today);
        System.out.println(reportDate + " | " + log);
    }

    @Override
    public void writeVerbose(String log, String id) {
        log(id + " | " + log);
    }

    @Override
    public void writeInformation(String log, String id) {
        log(id + " | " + log);
    }

    @Override
    public void writeWarning(String log, String id) {
        log(id + " | " + log);
    }

    @Override
    public void writeError(String log, Exception e, String id) {
        log(id + " | " + e.toString() + " | " + log);
    }
}
