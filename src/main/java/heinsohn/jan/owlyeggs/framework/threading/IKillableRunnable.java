package heinsohn.jan.owlyeggs.framework.threading;

public interface IKillableRunnable {
    void stop();
}
