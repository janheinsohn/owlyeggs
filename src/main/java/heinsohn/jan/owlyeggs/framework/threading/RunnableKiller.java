package heinsohn.jan.owlyeggs.framework.threading;

import java.util.ArrayList;

/**
 * Bietet allen Runnables, die IKillableRunnable implementieren die Möglichkeit,
 * sich für einen globalen Kill zu registrieren.
 * Führt dazu, dass beim Beenden der Anwendung alle Runnables beendet werden können.
 */
public class RunnableKiller {
    private static RunnableKiller ourInstance = new RunnableKiller();
    private ArrayList<IKillableRunnable> killableRunnables;

    public static RunnableKiller getInstance() {
        return ourInstance;
    }

    private RunnableKiller() {
        killableRunnables = new ArrayList<>();
    }

    /**
     * Registrierung für einen möglichen globalen Kill
     * @param killableRunnable Das Runnable, welches registriert werden soll
     */
    public void register(IKillableRunnable killableRunnable) {
        killableRunnables.add(killableRunnable);
    }

    /**
     * Beendet alle laufenden Runnables, die sich registriert haben
     */
    public void killAll() {
        killableRunnables.forEach(IKillableRunnable::stop);
    }
}
