package heinsohn.jan.owlyeggs.framework.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Provider für IoC Container
 */
public class Provider {
    private static Provider ourInstance = new Provider();
    private ApplicationContext context;

    /**
     * Holt sich die Instanz des Providers
     * @return Instanz
     */
    public static Provider getInstance() {
        return ourInstance;
    }

    /**
     * Holt sich den Context für IoC
     */
    private Provider() {
        context = new ClassPathXmlApplicationContext("spring/beans.xml");
    }

    /**
     * Stellt Beans zur Verfügung, die vorher Konfiguriert wurden
     * @param id Die ID der Bean
     * @param <T> Typ
     * @return Bean
     */
    @SuppressWarnings("unchecked")
    public <T> T provide(String id) {
        return (T) context.getBean(id);
    }

//    public static <T> T provide(String id) {
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring/beans.xml");
//        return (T) context.getBean(id);
//    }
}
